

														==================
														   OCAS ChatBOT
														==================

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */														


# What is OCAS ? 
=================
								 
 OCAS (Online Credit Approval System) is a specilized Loan Originating System devloped  by ERA-INFOTECH LIMITED. It automates the entire 
process of loan/credit including Document Management, Archiving and Approval System.

To know more please visit: http://www.erainfotechbd.com/product/ocas-online-credit-approval-system/


# What is OCAS ChatBOT ? 
==========================

 OCAS ChatBOT is a NLP based service which stimulates HCI (Human Computer Interaction) in a way that users can request various business
information, formulas and services related to OCAS. Dynamic MIS information for Management could also be accessiable accordingly. 

So, OCAS ChatBOT is actually a wrapper over OCAS which facilitates and provides real time business data to the authenticated users 
according to therir queries and needs.

Specially, Using a powerful web service written in flask OCAS bot can respond to authenticated users collecting information from external 
web services and even querying Databases!!


# Example:
===========
    
	USER: How many requests has been submitted by ARO in Gulshan Branch including agent points during last month ? 
	OCAS: Number of request submitted by ARO in Gulshan Branch during last month: 29.
	
	USER: Show me top 2 ARO in Banani Branch ? 
	OCAS: Sure, Here's the top 2 ARO in Banani Branch
		  1. User ID: ab.saidur@ocas.com,  Number of Approval: 40.
		  2. User ID: ab.shakhaoat@ocas.com,  Number of Approval: 36.
    .......
								 
								 

# Features:
==========

	1. Extended Database interaction capability.
	2. Fully supported with Python Request-Response mechanism.
	3. Could respond to multiple intents based on action parameters.
	4. Able to receive request parameters as JSON and prepare response for user intelligently. 
	5. Extract parametric values from user to generate sql query using different algoritms independently. 
	6. Nested requested could be invoked to interact external webservices as well as RESTFUL Web Services.
	7. User Authentication support added.
	8. SSL (Secured Socket Layer) included.
	
	
# Regards: 
==========

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	

