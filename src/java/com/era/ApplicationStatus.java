/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.era;

import com.connect.DBConnectionHandler;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Salman Rakin
 */

@WebServlet(name = "ApplicationStatus", urlPatterns = {"/ApplicationStatus"})
public class ApplicationStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ApplicationStatus</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ApplicationStatus at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sql = request.getParameter("q");
        String format = request.getParameter("format");
        String action= request.getParameter("act");
        String result = null;
        //String appl_type_code="'AI','DE','AP','AG";
        //String corp_flag="Y";
        String user_id=request.getParameter("usname");
        //user_id="'"+user_id+"'";
        String passwd=request.getParameter("paswd");
        //passwd="'"+passwd+"'";
        String company_code="'001'";
        
        //String query="SELECT DISTINCT appl_status_desc FROM ocasmn.vw_appl_sts_info WHERE application_id = 'DE16030144'";
        
        out.println(action);
        out.println(user_id); 
        out.println(passwd);
        out.println(sql);
        //out.println(query_procedure); 
        //String[] res= new String[500];
        
        ArrayList<String> res= new ArrayList<String>(); 
        
        //for(int i=0;i<60;i++) res[i]=null;
        
        //String[] res={};
        Connection con = DBConnectionHandler.getConnection();
        
        try {
           // PreparedStatement ps = con.prepareStatement("SELECT APPL_STATUS_DESC FROM ocasmn.vw_appl_sts_info "
             //       + " WHERE APPLICATION_ID=?");
          //  ps.setString(1, id);
          
          //ocasmn.dpr_oca_cmse_bot_info('001','shahrin@bankasia.net','123456',out_corporate_flg,out_appl_type_code,out_messages,out_code );
          
          
            CallableStatement statement = con.prepareCall("{call OCASMN.dpr_oca_cmse_bot_info(?, ?, ?, ?, ?, ?, ?)}");
           
            statement.registerOutParameter(4,Types.VARCHAR);
            statement.registerOutParameter(5, Types.VARCHAR);
            statement.registerOutParameter(6, Types.VARCHAR);
            statement.registerOutParameter(7, Types.INTEGER);
            
            statement.setString(1, company_code);
            statement.setString(2, user_id);
            statement.setString(3, passwd);
            
            statement.execute();
            
           
            String corp_flag = statement.getString(4);
            String appl_type_code = statement.getString(5);
            String out_message = statement.getString(6);
            Integer out_code=statement.getInt(7);
           
            out.println(corp_flag);
            out.println(appl_type_code);
            out.println(out_message);
            out.println(out_code);
            
            statement.close();
            
            if(action.equals("ApplicationStatus")||action.equals("Proposal.Count_AR")||action.equals("Proposal.Count")){
                 sql=sql+" AND application_type_code IN ("+appl_type_code+") AND createby = DECODE ('"+corp_flag+
                                "','N','"+user_id+"',createby)";
                    
            }
            else if(action.equals("Performance.individual")){
                
                String[] parts= sql.split("GROUP BY", 2);
                
                String str1=parts[0];
                String str2=parts[1];
                
                str1=str1+" AND application_type_code IN ("+appl_type_code+") AND createby = DECODE ('"+corp_flag+
                                "','N','"+user_id+"',createby)";
                str2=" GROUP BY "+ str2;
                
                sql=str1+str2;
                    
            }
            
            out.println(sql);
          
            PreparedStatement ps = con.prepareStatement(sql);
            //ps.executeUpdate();
            
            OrderedJSONObject json= new OrderedJSONObject();
            LinkedHashMap<String, Map<String,Object>> map_json= new LinkedHashMap<String, Map<String,Object>>();
            
            
            if(out_code==1){
                
                    Map<String, Object> map1 = new LinkedHashMap<String, Object>();
                    List<Map<String, Object>> list1 = new ArrayList<Map<String, Object>>();
                    
                    if(action.equals("ApplicationStatus")){
                        result=out_message;
                        //String flag= corp_flag;
                        map1.put("flag", corp_flag);
                        map1.put("result", result);
                       
                        //list1.add(map1);
                        json.put("Status", map1);
                        out.println(json);
                    }
                    else if(action.equals("Proposal.Count_AR")||action.equals("Proposal.Count")){
                        result=out_message;
                        map1.put("flag","1");
                        map1.put("result", result);
                        
                        //list1.add(map1);
                        
                      
                        json.put("Status", map1);
                    }
                    
                     else if(action.equals("Performance.individual")){
                        
                        json.put("Result", out_message);
                    }
                    
                    
            
            }
            
            //JSONObject json = new JSONObject();
            else{ 
                 
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            int j=1;
            int row_count=1;
            int loop_count=0;
            
            
            
            
            
            while(rs.next()){
                //result = rs.getString("APPL_STATUS_DESC");
                out.println("Entered into Root......");
                loop_count++;
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                
                if(action.equals("Proposal.Count")||action.equals("Proposal.Count_AR")){ // how many proposals approved
                
                    result=rs.getString("N0_OF_PROPOSAL");
                    map.put("flag","0");
                    map.put("result", result);
                    //list.add(map);
                    json.put("Status",map);
                    out.println(json);
                }  
                       
                
                else if (action.equals("Performance.individual")){
                    
                   for (int i = 1; i <= columnsNumber; i++) {
                       
                        res.add(rs.getString(i)); //=rs.getString(i);
                        map.put(rsmd.getColumnName(i), res.get(j-1).trim());
                        j++;
                       
                    }
                    
                    //list.add(map);
                    //map_json.put("Row"+row_count, map);
                    
                     json.put("Row"+row_count, map);
                
                    row_count++;
                
                }
                       
                else if (action.equals("ApplicationStatus"))
                {
                    out.println("Dhukse...........");
                    
                    result = rs.getString("APPL_STATUS_DESC"); // Application status from id
                    //if (result==" ")
                    //    result="NIL";
                    map.put("flag", corp_flag);
                    map.put("result", result);
                   
                    //list.add(map);
                    json.put("Status", map);
                    out.println(json);
                
                }
            }
//            list.add(map);
//            json.put("Status", list);
            
            if (action.equals("Performance.individual")){
                
                OrderedJSONObject json_final= new OrderedJSONObject();
                json_final.put("Number of Rows",row_count-1);
                json_final.put("Query",json);
                json_final.put("Result","OK");
                json=json_final;
                
            }
            
                if(loop_count==0){
                   if (action.equals("ApplicationStatus")){
                       
                       Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                       List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
                       
                        map2.put("flag", corp_flag);
                        map2.put("result", result);
                        
                        json.put("Status", map2);
                        out.println(json);

                   } 
                   
                   else if (action.equals("Performance.individual")){
                       
                       out_message="Sorry! No record found for the data you specified! ";
                       json.put("Result", out_message);

                   } 

                }
            }
            
            
            /*
            
            if (action.equals("Performance.individual")){ // watch closely 
                
                
                JSONObject orderedJson= new JSONObject(map_json);
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(Arrays.asList(orderedJson));
              
            
            
                //JSONObject json= new JSONObject();
                json.put("Number of Rows",row_count-1);
                json.put("Query",jsonArray);
            
                
            
            }
                
            */
            
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.setCharacterEncoding("UTF-8");
            
            if("json".equals(format)){
                response.setContentType("application/json");
                response.getWriter().print(json.toString());
            }
            else{
                 response.setContentType("text/plain");
                response.getWriter().print(result);
            }
            response.getWriter().flush();
            
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(ApplicationStatus.class.getName()).log(Level.SEVERE, null, ex);
        }
        DBConnectionHandler.releaseConnection(con);
          
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
